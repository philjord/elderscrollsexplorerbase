# ElderScrollsExplorerBase #

This is a common base for ESEAndroid-apk to build on, it contains no awt and no Android gui components

It will however run a pure 3D window version of ElderScrollsExplorer usng the Jogl Newt window system

For more information please see https://github.com/philjord/ElderScrollsExplorer